# Judul Skripsi Anda

Samuel Tupa Febrian - 1606878713 - Ilmu Komputer Universitas Indonesia (Sarjana, 2016)

----------

## Selayang Pandang
*Payment gateway* merupakan salah satu metode transaksi uang yang memproses pembayaran dan konfirmasinya secara otomatis. Berdasarkan hal tersebut, *payment gateway* dapat digunakan untuk membantu organisasi amal untuk mengelola donasi dan distribusi dalam bentuk uang. Setiap organisasi amal memiliki preferensi *payment gateway* yang berbeda, sehingga diperlukan suatu metode pengembangan perangkat lunak untuk mengakomodasi kebutuhan tersebut. Salah satu metode yang dapat digunakan adalah paradigma *Software Product Line Engineering* (SPLE). Pengembangan situs *web* organsisai amal menggunakan paradigma SPLE direalisasikan dalam sebuah studi kasus bernama AISCO. Penelitian ini mengembangkan *back-end* dari fitur *payment gateway* pada AISCO menggunakan ABS-*Microservices Framework*. Untuk menguji hasil pengembangan, dilakukan eksperimen terhadap studi kasus berupa yayasan yang menggunakan produk AISCO. Pengembangan fitur *payment gateway* menggunakan paradigma SPLE diharapkan dapat mengurangi *effort* yang dibutuhkan untuk pengembangan fitur tersebut pada variasi produk AISCO.

## Ucapan Terima Kasih
Terkhusus kepada seluruh anggota Lab Reliable Software Engineering (RSE), serta semua pihak lain yang terlibat secara tidak langsung dalam pengerjaan (secara penulisan, *proofreading*, atau psikologis). Ucapan terima kasih lebih khususnya dapat dilihat pada bagian kata pengantar.

## *Milestone* Pekerjaan
Dapat dilihat pada bagian *Issues* di *sidebar* halaman repositori ini di GitLab.

## Kumpulan Referensi yang Digunakan (*Under Development*)
Berikut ini kumpulan *softcopy* referensi yang saya gunakan untuk skripsi ini, yang diperbolehkan untuk disebarluaskan secara bebas: Sertakan link Anda.

Daftar referensi yang tersedia (akan diperbaharui hingga akhir pengerjaan):
1.  Judul (Tahun)

Disclaimer: Konten referensi yang diberikan melalui link tersebut adalah apa adanya tanpa ada perubahan apapun. Saya hanya membagikan referensi tersebut jika tidak ada lisensi yang melarang saya untuk membagikannya secara luas.

## README Template
2010-12-23
Modified on 2019-11-26


Template ini dibuat oleh Andreas Febrian (Fasilkom UI 2003)
dan disebarluaskan oleh Erik Dominikus (Fasilkom UI 2007).

Template ini dimodifikasi oleh Azhar Kurnia (Fasilkom UI 2016)
dan Ichlasul Affan (Fasilkom UI 2016) agar mengikuti standar terbaru Tugas Akhir di Universitas Indonesia (tahun 2017).

Orang yang membaca karya Anda tidak peduli cara Anda buat karya itu;
mereka hanya mau tahu karya itu bagus.

Selamat menggunakan template ini.
