package MMidtransSnapRedirectImpl;

// import library

public class MidtransSnapRedirectImpl_fli extends MidtransSnapRedirectImpl_c { 

    public Map<String, Object> requestBody(String order_id, String amount) {
        Map<String, Object> params = new HashMap<>();

        Map<String, String> transactionDetails = new HashMap<>();
        transactionDetails.put("order_id", order_id);
        transactionDetails.put("gross_amount", amount);

        Map<String, String> creditCard = new HashMap<>();
        creditCard.put("secure", "true");

        params.put("transaction_details", transactionDetails);
        params.put("credit_card", creditCard);

        return params;
    }

	@Override
    public ABSString fli_get_redirect_string(ABSString server_key, ABSString client_key, ABSString order_id, ABSString amount) {
        String server_keyjava = server_key.getString();
        String client_keyjava = client_key.getString();
        String order_idjava = order_id.getString();
        String amount_java = amount.getString();

        MidtransSnapApi snapApi = new ConfigFactory(new Config(server_keyjava, client_keyjava, false)).getSnapApi();

        String redirectURL = snapApi.createTransactionRedirectUrl(this.requestBody(order_idjava, amount_java));

        return ABSString.fromString(redirectURL);
    } 

    @Override
    public ABSString fli_handle_notification(ABSString server_key, ABSString client_key, ABSString order_id) {
        String server_keyjava = server_key.getString();
        String client_keyjava = client_key.getString();
        String orderId = order_id.getString();

        MidtransCoreApi coreApi = new ConfigFactory(new Config(server_keyjava, client_keyjava, false)).getCoreApi();

        String transactionStatus = null;
        String fraudStatus = null;
		
        try {
            JSONObject transactionResult = coreApi.checkTransaction(orderId);

            transactionStatus = (String) transactionResult.get("transaction_status");
            fraudStatus = (String) transactionResult.get("fraud_status");
        } catch (JSONException e) {
            transactionStatus = "NA";
            fraudStatus = "NA";
        }

        if (transactionStatus.equals("capture") || transactionStatus.equals("settlement")) {
            if (fraudStatus.equals("challenge")) {
                return ABSString.fromString("CHALLENGE");
            } else if (fraudStatus.equals("accept")) {
                return ABSString.fromString("SUCCESS");
            }
        } else if (transactionStatus.equals("cancel") || transactionStatus.equals("deny") || transactionStatus.equals("expire")) {
            return ABSString.fromString("FAIL");
        } else if (transactionStatus.equals("pending")) {
            return ABSString.fromString("PENDING");
        }
        return ABSString.fromString("ERROR");
    } 
}
