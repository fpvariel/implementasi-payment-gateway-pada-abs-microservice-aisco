import datetime
import requests
import json
import uuid
from hashlib import sha1
from .params import get_doku_donation_param_api, get_doku_distribution_param_api
from .models import Donation, Income, Distribution
from django.core import serializers

def get_all_donations():
	'''
	implementasi proses pengambilan seluruh catatan transaksi donation
	biaya produksi: 17 baris
	'''
	
def get_all_distributions():
	'''
	implementasi proses pengambilan seluruh catatan transaksi distribution
	biaya produksi: 15 baris
	'''

def generate_donation_param_doku(request):
	'''
	implementasi proses pembuatan payload untuk reidrect ke halaman DOKU
	biaya produksi: 51 baris
	'''

def handle_notify_doku(request):
	'''
	implementasi proses penanganan notifikasi DOKU
	biaya produksi: 38 baris
	'''
	
def generate_distribution_doku(request):
	'''
	implementasi proses pembuatan transaksi distribution DOKU
	biaya produksi: 76 baris
	'''

def handle_remit_doku(request):
	'''
	implementasi proses remitansi transaksi distribution DOKU
	biaya produksi: 99 baris
	'''
	
def non_post_msg():
	'''
	implementasi proses penanganan request yang tidak valid
	biaya produksi: 2 baris
	'''