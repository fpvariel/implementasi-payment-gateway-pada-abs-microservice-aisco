# import library lain dari template
from django.conf.urls import include
import payment_gateway.urls as payment_gateway

urlpatterns = [
    ...,
	path('api/payment-gateway/', include(payment_gateway, namespace='payment_gateway'))
	...
]