package com.rse.absserver;

import com.rse.middleware.ABSServlet;
import java.io.File;
import java.io.PrintStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.MultipartConfigElement;
import javax.servlet.ServletException;
import org.apache.catalina.Context;
import org.apache.catalina.LifecycleException;
import org.apache.catalina.Server;
import org.apache.catalina.startup.Tomcat;


public class ABSServer {
  public ABSServer() {}

  public static void main(String[] args) {
    ABSServer server = new ABSServer();
    server.run();
  }

  public void run() {
    run("10001");
  }

  public void run(String port) {
    // implementasi runserver
  }
}
