package MAPIAdaptorImpl;

import abs.backend.java.lib.types.ABSString;
import abs.backend.java.lib.types.ABSInteger;
import abs.backend.java.lib.types.ABSUnit;
import abs.backend.java.lib.types.ABSValue;
import ABS.StdLib.Pair;
import ABS.StdLib.Pair_Pair;
import abs.backend.java.lib.runtime.FLIHelper;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.net.ProxySelector;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.rse.middleware.DataTransformer;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.entity.StringEntity;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.SystemDefaultRoutePlanner;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class APIAdaptorImpl_fli extends APIAdaptorImpl_c { 

    public void putWithNestedKey(String key, String value, JSONObject json) {
        String[] splited_keys = key.split("\\.");
        for (String key_level: splited_keys) System.out.println(key_level);
        int lastIdx = splited_keys.length - 1;
        JSONObject currentJson = json;
        try {
            for (int i=0;i<lastIdx;i++) {
                String currentKey = splited_keys[i];
                if (!currentJson.has(currentKey)) currentJson.put(currentKey, new JSONObject());
                currentJson = currentJson.getJSONObject(currentKey);
            }
            currentJson.put(splited_keys[lastIdx], value);
        } catch (JSONException e) {}
    }

    public void putPairOnBody(String key, String value, JSONObject body) {
        if (key.contains("NESTED")) {
            String nestedKey = key.split("_")[1];
            putWithNestedKey(nestedKey, value, body);
        } else {
            try {
                body.put(key, value);
            } catch (JSONException e) {}
        }
    }

	@Override
    public ABS.StdLib.Map<ABSString, ABSString> fli_call_reqres_POST(ABS.StdLib.Map<ABSInteger, Pair<ABSString, ABSString>> payload) { 
        HashMap<String, String> param = DataTransformer.convertABSMapToJavaMap(payload);
        HashMap<String, String> headers = new HashMap<>();
        String contentType = "";
		
        if (param.containsKey("HEADER_Content-Type")) {
            contentType = param.get("HEADER_Content-Type");
        } 
        String endpoint = "";

        JSONObject body = new JSONObject();

        List<StringPair> urlParameters = new ArrayList<>();

        for (Map.Entry<String, String> api_payload : param.entrySet()) {
        	String key = api_payload.getKey();
            String value = api_payload.getValue();

            if (key.equals("ENDPOINT")){
                endpoint = value;
            } else {
                urlParameters.add(new StringPair(key, value));
            }
        }

        String json_result = "";
        HttpPost post = new HttpPost(endpoint);
	
        try {
            post.setEntity(new UrlEncodedFormEntity(urlParameters));
        }catch(UnsupportedEncodingException e){}
		
        try {    	
            SystemDefaultRoutePlanner routePlanner = new SystemDefaultRoutePlanner(ProxySelector.getDefault());
            CloseableHttpClient httpClient = HttpClientBuilder.create()
                .setRoutePlanner(routePlanner).build();
     	
            CloseableHttpResponse response = httpClient.execute(post);
            json_result = EntityUtils.toString(response.getEntity());
        }catch(IOException e) {}

        ABS.StdLib.Map<ABSString, ABSString> result = null;
        try {
            result = DataTransformer.convertJSONToABSMap(json_result);        	
        }catch (JSONException e) {}
		
        return result;
    } 
	
    @Override
    public ABSString fli_call_reqres_GET(ABSString host, ABSInteger port, ABSString scheme, ABSString request) {  
        String hostjava = host.getString();
        int portjava = port.toInt();
        String schemejava = scheme.getString();
        String requestjava = request.getString();

        String result = "";
        SystemDefaultRoutePlanner routePlanner = new SystemDefaultRoutePlanner(ProxySelector.getDefault());
        CloseableHttpClient httpclient = HttpClientBuilder.create()
            .setRoutePlanner(routePlanner).build();
        try {
          HttpHost target = new HttpHost(hostjava, portjava, schemejava);
          HttpGet getRequest = new HttpGet(requestjava);
          HttpResponse httpResponse = httpclient.execute(target, getRequest);
          HttpEntity entity = httpResponse.getEntity();

          if (entity != null) {
            result = EntityUtils.toString(entity);
          }
        } catch (Exception e) {
          e.printStackTrace();
        } finally {
          httpclient.getConnectionManager().shutdown();
        }

        return ABSString.fromString(result);
    } 
}

class StringPair extends BasicNameValuePair{
	public String first;
	public String second;
	public StringPair(String name, String value) {
		super(name, value);
		this.first = name;
		this.second = value;
		// TODO Auto-generated constructor stub
	}
}