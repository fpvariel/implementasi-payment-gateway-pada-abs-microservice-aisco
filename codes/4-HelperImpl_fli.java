package MHelperImpl;

// import library

public class HelperImpl_fli extends HelperImpl_c { 
    @Override
    public ABSString fli_timestamp() { 
    	String result = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss.SSSSSS").format(new Date());
    	return ABSString.fromString(result);
    } 	
    @Override
    public ABSString fli_datenow() { 
        String result = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
        return ABSString.fromString(result);
    }
    @Override
    public ABSString fli_dateonlynow() { 
        String result = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        return ABSString.fromString(result);
    }	
    @Override
    public ABSUnit fli_echo(ABSString input) { 
        System.out.println(input.getString());
        return ABSUnit.UNIT;
    }
    @Override
    public ABS.StdLib.Map<ABSString, ABSString> fli_parse_json(ABSString json) { 
        String json_java = json.getString();
        try {
            return DataTransformer.convertJSONToABSMap(json_java);
        } catch (JSONException e) {
            return new ABS.StdLib.Map_EmptyMap<ABSString, ABSString>();
        }
    }
    @Override
    public ABSString fli_uuid() {
        String result = UUID.randomUUID().toString();
        return ABSString.fromString(result);
    }
	@Override
    public ABSString fli_hash(ABSString input) { 
    	String result = DigestUtils.sha1Hex(input.getString());
        return ABSString.fromString(result);
    } 
    @Override
    public ABSString fli_base64_encode(ABSString input) { 
    	String result = Base64.getEncoder().encodeToString(input.getString().getBytes());
        return ABSString.fromString(result);
    }
    @Override
    public ABSString fli_encrypt(ABSString valueToEnc, ABSString encKey) {
        String ALGORITHM = "AES";
        try {
            Key key = new SecretKeySpec(encKey.getString().getBytes(), ALGORITHM);
            Cipher c = Cipher.getInstance(ALGORITHM);
            c.init(Cipher.ENCRYPT_MODE, key);
            byte[] encValue = c.doFinal(valueToEnc.getString().getBytes());
            String result = Base64.getEncoder().encodeToString(encValue);
            return ABSString.fromString(result);
        } catch (Exception e) {
            return ABSString.fromString("");
        }
	}
}
