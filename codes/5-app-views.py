import requests
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from .services import get_all_donations, generate_donation_param_doku, handle_notify_doku
from .services import get_all_distributions, generate_distribution_doku, handle_remit_doku

# Create your views here.
def donation_list(request):
	return JsonResponse(get_all_donations(), safe=False)
	
def distribution_list(request):
	return JsonResponse(get_all_distributions(), safe=False)

@csrf_exempt
def get_donation_param_doku(request):
	return JsonResponse(generate_donation_param_doku(request))

@csrf_exempt
def notify_doku(request):
	return JsonResponse(handle_notify_doku(request))

@csrf_exempt
def create_distribution_doku(request):
	return JsonResponse(generate_distribution_doku(request))
	
@csrf_exempt
def remit_doku(request):
	return JsonResponse(handle_remit_doku(request))